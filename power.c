/*
  Measure Volts.c
  Make voltmeter style measurements with the Propeller Activity Board.
  http://learn.parallax.com/propeller-c-simple-circuits/measure-volts
*/

#include "simpletools.h"                      // Include simpletools
#include "adcDCpropab.h"                      // Include adcDCpropab

int main()                                    // Main function
{
  adc_init(21, 20, 19, 18);                   // CS=21, SCL=20, DO=19, DI=18

  float v3, P1, C, P2;                        // Voltage variables

  while(1)                                    // Loop repeats indefinitely
  {
    v3 = adc_volts(3); 
    C = v3/217;                               // Check A/D 3
    P1 = ((v3)*(v3))/ 217;                    // Power (V^2)/R
    P2 = (C*C)*217;                           // Power (C^2)*R
    putChar(HOME);                            // Moves cursor to top-left "home"
    print("A/D3 = %f V%c\n", v3, CLREOL);     // Display volts
    print("Power = %f W%c\n", P1, CLREOL);
    print("Power = %f W%c\n", P2, CLREOL);
    pause(500);                               // Wait 1/2 s
  }  
}
